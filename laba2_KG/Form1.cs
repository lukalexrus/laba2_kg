﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

//http://habrahabr.ru/post/173131/
namespace laba2_KG
{
    public partial class Form1 : Form
    {
        private bool loading = false;//загружаем GL до конца
        private KeyValuePair<double, double>[] spisok = new KeyValuePair<double, double>[500];//список содержащий координаты
        private double angle = 0;//поворот
        private double xsdwig = 1;//движение по x
        private double a = 1;
        private double b = 3;
        double x;


        public Form1()
        {
            InitializeComponent();
            trackBar1.Minimum = 1;
            trackBar1.Maximum = 10;
            trackBar2.Minimum = 1;
            trackBar2.Maximum = 10;
            trackBar3.Minimum = 1;
            trackBar3.Maximum = 20;
        }

        private void line()
        {
            GL.Color3(Color.CadetBlue);
            GL.Begin(BeginMode.LineStrip);

            GL.Vertex2(0, 100);
            GL.Vertex2(2000, 100);

            GL.End();
        }


        private double calccoord(double par, double dvig)
        {
            for (int i = 0; i < 500; i++)
            {
                x = (a * Math.Cos(Math.PI * par / 180) * Math.Cos(Math.PI * i / 180) - b * Math.Sin(Math.PI * par / 180) * Math.Sin(Math.PI * i / 180));// / 10;///180.0*M_PI
                //x += par - Math.Sin(Math.PI * par / 180);
                double y = (a * Math.Sin(Math.PI * par / 180) * Math.Cos(Math.PI * i / 180) + b * Math.Cos(Math.PI * par / 180) * Math.Sin(Math.PI * i / 180)); /// 10;
                y *= 20;
                x *= 20;
                x += dvig;
                // Console.Write(x);
                
                spisok[i] = new KeyValuePair<double, double>(x, y);
            }

            return spisok.Min(t =>
            {
                return t.Value;
            });
        }


        double finish = 0;

        private void drawElips(double par, double dvig)
        {
            double min = calccoord(par, dvig);//минимальное растояние
            GL.ClearColor(Color.Aqua);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.PointSize(3);
            line();
            GL.Begin(BeginMode.Points);
            var tmp = spisok[11];
            double start = 0;
            foreach (var t in spisok)
            {
                if (t.Key==spisok.Last().Key)
                {

                }
                else
                {
                    if (t.Value + 100 - min < 100.5 && t.Value + 100 - min > 100)
                    {
                        GL.Color3(Color.White);
                        GL.Vertex2(t.Key + 50, t.Value + 100 - min);
                        if(start==0)
                        {
                            start =  t.Key + 50;
                        }
                        else
                        {
                            finish = t.Key + 50 - start;
                        }
                        
                    }
                    else
                    {
                        if(finish!=0)
                        {
                            xsdwig += finish;
                           finish = 0;
                        }
                        start = 0;
                        GL.Color3(Color.DeepPink);
                        GL.Vertex2(t.Key + 50, t.Value + 100 - min);
                    }
                }
            }
            GL.End();
            GL.PointSize(5);
            GL.Begin(BeginMode.Points);
                    GL.Color3(Color.Black);
                    GL.Vertex2(spisok[10].Key + 50, spisok[10].Value + 100 - min);
            GL.End();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            if (loading)
            {
                drawElips(angle, xsdwig);
                line();
                timer1.Interval = 100;
                timer1.Tick += timer1_Tick;
                timer1.Start();
                glControl1.SwapBuffers();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            xsdwig += finish;
            finish = 0;
            angle -= trackBar3.Value;
            drawElips(angle, xsdwig);
            line();
            glControl1.SwapBuffers();
        }

        private void glControl1_Load(object sender, EventArgs e)
        {
            loading = true;
            int w = glControl1.Width;
            int h = glControl1.Height;
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, w, 0, h, -1, 1); // Верхний левый угол имеет кооординаты(0, 0)
            GL.Viewport(0, 0, w, h); // Использовать всю поверхность GLControl под рисование
            drawElips(angle, xsdwig);
            glControl1.SwapBuffers();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            textBox1.Text = trackBar1.Value.ToString();
            drawElips(angle, xsdwig);
            glControl1.SwapBuffers();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            textBox2.Text = trackBar2.Value.ToString();
            drawElips(angle, xsdwig);
            glControl1.SwapBuffers();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           timer1.Tick-=timer1_Tick;
            timer1.Stop();
            angle = 0;//поворот
            xsdwig = 1;//движение по x
            drawElips(angle, xsdwig);
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            label3.Text = trackBar3.Value.ToString();
        }
    }
}